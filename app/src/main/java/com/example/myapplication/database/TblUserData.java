package com.example.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TblUserData extends Mydatabase {

    public static final String TBL_USERDATA = "Tbl_UserData";
    public static final String USERID = "UserId";
    public static final String NAME = "Name";
    public static final String PHONENUMBER = "PhoneNumber";
    public static final String EMAIL = "Email";
    public static final String GENDER = "gender";
    public static final String HOBBIES = "hobbies";

    public TblUserData(Context context) {
        super(context);
    }

    public long insertUserDetail(String name, String phonenumber, String email) {
        long insertedId = 0;
        if (isNumberAvaliable(phonenumber)) {
            insertedId = -1;
        } else {
            SQLiteDatabase db = getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(NAME, name);
            cv.put(PHONENUMBER, phonenumber);
            cv.put(EMAIL, email);
            insertedId = db.insert(TBL_USERDATA, null, cv);
            db.close();
        }
        return insertedId;
    }

    public boolean isNumberAvaliable(String phonenumber) {
        SQLiteDatabase db = getReadableDatabase();
        String query = "SELECT * from " + TBL_USERDATA + " WHERE " + PHONENUMBER + " = ? ";
        Cursor cursor = db.rawQuery(query, new String[]{phonenumber});
        cursor.moveToFirst();
        boolean isNumberAvaliable = cursor.getCount() > 0;
        cursor.close();
        db.close();
        return isNumberAvaliable;
    }
}
